package com.utkarsh.myapplication.domain;

/**
 * Created by NEXTGN001 on 17/01/2018.
 */

public class ApiCaller {
    private static ApiCaller apiCaller;
    public static ApiCaller getCaller() {
        if (apiCaller == null)
            apiCaller = new ApiCaller();
        return apiCaller;
    }

    public String getMapDirections(String url) {
        return ServerInteractor.httpGetRequestToServer(url);
    }
}
