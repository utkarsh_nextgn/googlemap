package com.utkarsh.myapplication.domain;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.net.URI;
import java.net.UnknownHostException;

/**
 * Created by NEXTGN001 on 17/01/2018.
 */

public class ServerInteractor {

    public static String httpGetRequestToServer(String URLs) {

        //http://125.454.54.4/ksm/login?username=abc&password=12324
        HttpClient client = new DefaultHttpClient();

        HttpGet request = new HttpGet();

        Log.w("url", URLs);
        try {
            URI url = new URI(URLs.replace(" ", "%20"));
            request.setURI(url);
            HttpResponse response = client.execute(request);
            String responseEntity = EntityUtils.toString(response.getEntity());
            Log.w("server resp:", responseEntity);
            return responseEntity;
        } catch (UnknownHostException e) {
            return "UnknownHostException";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";
        }
    }
}
