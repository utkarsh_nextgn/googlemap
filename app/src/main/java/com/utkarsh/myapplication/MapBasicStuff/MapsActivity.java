package com.utkarsh.myapplication.MapBasicStuff;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.utkarsh.myapplication.NearByPlaceSearch.GooglePlacesActivity;
import com.utkarsh.myapplication.R;
import com.utkarsh.myapplication.domain.ApiCaller;
import com.utkarsh.myapplication.domain.Constant;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {
    private static final String GOOGLE_API_KEY = "AIzaSSDFSDF8Kv2eP0PM8adf5dSDFysdfas323SD3HA";
    private GoogleMap mMap;
    private LocationManager locationManager;
    private String provider;
    public double lat, lng;
    Location location;
    LatLng latLng,latLng1;
    LatLngBounds bounds;
    private Polyline currDirection;
    String address;
    String city;
    String state;
    String country;
    String postalCode;
    String knownName;
    private android.support.v7.widget.Toolbar toolbar;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        imageView =(ImageView)findViewById(R.id.menuimage);
        imageviewClicked();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
             location = locationManager.getLastKnownLocation(provider);
        }

    }

    private void imageviewClicked() {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void showPopup() {
        PopupMenu menu = new PopupMenu(this, toolbar);
        menu.inflate(R.menu.mapmenu);


        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.hybrid:
                        Intent intent = new Intent(MapsActivity.this, GooglePlacesActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.normal:
                        return true;
                    case R.id.satelite:
                        return true;
                    case R.id.terrain:

                        return true;



                }
                return false;
            }

        });
        @SuppressLint("RestrictedApi") MenuPopupHelper menuHelper = new MenuPopupHelper(this, (MenuBuilder) menu.getMenu(), toolbar);
        menuHelper.setForceShowIcon(true);
        menuHelper.setGravity(Gravity.RIGHT);
        menuHelper.show();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(mMap!=null){
            if (location != null) {
                onLocationChanged(location);
            } else {

            }
        }
        LatLng latLng = new LatLng(lat, lng);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }else {
           // mMap.setMyLocationEnabled(true);
        }
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();
        if(mMap!=null){
            latLng = new LatLng(lat,lng);
            mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.currentmarker))
                    .anchor(0.01f,2.1f)
                  .title("You Re Here")
                    .position(latLng));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        }if(mMap!= null){
            latLng1 = new LatLng(27.1767,78.0081);
            mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                    .anchor(0.01f,2.1f)
                    .title("You go Here")
                    .position(latLng));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        }
        setBoundOnMap();
    }

    public void getMarkerNames(double lat, double lng) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        if(mMap!=null){
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
            knownName = addresses.get(0).getFeatureName();

        }if(mMap!=null){
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(27.1767, 78.0081, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        }
    }
    private void setBoundOnMap() {
        Marker pickupMarker,dropMarker;
        mMap.clear();
        try {
            getMarkerNames(lat,lng);
        } catch (IOException e) {
            e.printStackTrace();
        }
        pickupMarker = mMap.addMarker(new MarkerOptions().position(
                new LatLng(lat, lng)).icon(
                BitmapDescriptorFactory.fromResource(R.drawable.currentmarker)).title(city+", "+state+", "+country));
        pickupMarker.setAnchor(0.5f, 0.5f);
        pickupMarker.showInfoWindow();
        dropMarker = mMap.addMarker(new MarkerOptions().title(address).position(
                new LatLng(27.1767, 78.0081)).icon(
                BitmapDescriptorFactory.fromResource(R.drawable.marker)));
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        boundsBuilder.include(latLng);
        boundsBuilder.include(latLng1);
         bounds = boundsBuilder.build();
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 200
                );
                mMap.moveCamera(cu);
                mMap.animateCamera(cu);
                createPolyLineToDestination(latLng,latLng1);
            }
        });

    }
    private void createPolyLineToDestination(LatLng from, LatLng to) {
        if (from == null || to == null) {
            return;
        }
        new AsyncTask<String, Void, List<List<HashMap<String, String>>>>() {
            @Override
            protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
                JSONObject jObject;
                List<List<HashMap<String, String>>> routes = null;
                try {
                    String res = ApiCaller.getCaller().getMapDirections(strings[0]);
                    Log.d("ParserTask", strings[0]);
                    if (res.matches("UnknownHostException")) {
                        return null;
                    }
                    jObject = new JSONObject(res);
                    DataParser parser = new DataParser();
                    Log.d("ParserTask", parser.toString());
                    // Starts parsing data
                    routes = parser.parse(jObject);
                    Log.d("ParserTask", "Executing routes");
                    Log.d("ParserTask", routes.toString());
                } catch (Exception e) {
                    Log.d("ParserTask", e.toString());
                    e.printStackTrace();
                }
                return routes;
            }

            @Override
            protected void onPostExecute(List<List<HashMap<String, String>>> result) {
                ArrayList<LatLng> points;
                PolylineOptions lineOptions = null;
                if (currDirection != null)
                    currDirection.remove();
                if (result == null) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(MapsActivity.this);
                    dialog.setMessage(getResources().getString(R.string.no_working_connection));
                    dialog.setPositiveButton(getResources().getString(R.string.okay), null);
                    dialog.create().show();
                    return;
                }
                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();
                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);
                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(Color.RED);
                    Log.d("onPostExecute", "onPostExecute lineoptions decoded");
                }

                // Drawing polyline in the Google Map for the i-th route
                if (lineOptions != null) {
                    currDirection = mMap.addPolyline(lineOptions);
                } else {
                    Log.d("onPostExecute", "without Polylines drawn");
                }
            }
        }.execute("https://maps.googleapis.com/maps/api/directions/json?origin=" + from.latitude + ","
                + from.longitude + "&destination=" + to.latitude + ","
                + to.longitude +
                "&key=" + Constant.GoogleApiKey);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
